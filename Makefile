#fake Makefile for nemesis, to support the common
# ./configure;make;make install

PYTHON = python

#build: Setup setup.py
build: setup.py
	$(PYTHON) setup.py build


osx: setup.py
	$(PYTHON) setup.py py2app
	ditto --rsrc --arch i386 dist/nemesis.app dist/nemesis.app.i386
	rm -r dist/nemesis.app
	mv dist/nemesis.app.i386 dist/nemesis.app


#install: Setup setup.py
install: setup.py
	$(PYTHON) setup.py install

clean:
	rm -rf build dist MANIFEST .coverage
	rm -f nemesis/*~
	rm -rf bin develop-eggs eggs parts .installed.cfg nemesis.egg-info
	find . -name *.pyc -exec rm {} \;
	find . -name *.swp -exec rm {} \;
	$(PYTHON) setup.py clean

#upload to pypi
upload:
	make clean
	#if you have your gpg key set up... sign your release.
	#$(PYTHON) setup.py sdist upload --sign --identity="Your Name <youremail@example.com>" 
	$(PYTHON) setup.py sdist upload

sdist:
	make clean
	$(PYTHON) setup.py sdist

