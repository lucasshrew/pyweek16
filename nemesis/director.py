
import pygame

from pygame.locals import *
from pgu import gui

class Director(object):

    _director = None

    @classmethod
    def get(cls):
        if not cls._director:
            cls._director = cls()
        return cls._director

    def __init__(self):
        self._stack = []
        self._current = None
        
        # PGU gui initialization
        self._app = gui.App()
        self._form = gui.Form()
        self._container = gui.Container(align=-1, valign=-1)
        self._app.init(self._container)

    def push(self, scene):
        if self._stack:
            old = self._stack[-1]
            old.on_hide()

        self._stack.append(scene)
        scene.on_enter()

    def pop(self):

        if not self._stack:
            return

        old = self._stack.pop()
        old.on_exit()

        if self._stack:
            self._stack[-1].on_show()
        else:
            exit(0)

    def replace(self, scene):
        if self._stack:
            old = self._stack.pop()
            old.on_exit()

        self._stack.append(scene)
        scene.on_enter()

    def update(self, dt):
        if self._stack:
            self._stack[-1].update(dt)
        
        for e in pygame.event.get():
            self._app.event(e)

    def draw(self, screen):
        if self._stack:
            self._stack[-1].draw(screen)

        self._app.paint()

    def get_root_widget(self):
        return self._container

    def update_root_widget(self, widget):
        self._app.remove(self._container)
        self._app.add(widget, 0, 0)
        self._container = widget

    def remove_root_widget(self):
        self._app.remove(self._container)


class Scene(object):
    
    def __init__(self):
        pass

    def on_enter(self):
        pass

    def on_exit(self):
        pass

    def on_show(self):
        pass

    def on_hide(self):
        pass

    def update(self, dt):
        pass

    def draw(self, screen):
        pass

