import sys

import pygame, time, random, math, os
from pygame.locals import *

import data
import director
import scenes

class Screen(object):
    
    width = 800
    height = 600
    
    def __init__(self):
        size = (self.width, self.height)
        self._surface = pygame.display.set_mode(size, pygame.DOUBLEBUF)
    
    def clear(self):
        self._surface.fill((180, 180, 180))

    def blit(self, surf, dest=(0,0)):
        self._surface.blit(surf, dest)


def main():
    pygame.init()
    pygame.display.set_caption("Nemesis")
    screen = Screen()
    game = director.Director.get()
    _ = scenes.Intro()
    run(game, screen)


'''
    Game main loop
'''
def run(game, screen):
    clock = pygame.time.Clock()
    while True:
        check_quit()
        update(game, clock.get_time())
        draw(game, screen)
        clock.tick(30)


'''
    Exit condition check
'''
def check_quit():
    pressed = pygame.key.get_pressed()
    if pressed[K_ESCAPE] or pygame.event.get(QUIT):
            exit_game()


'''
    update logic
'''
def update(game, dt):
    if dt > 200: dt = 33
    game.update(dt)


'''
    draw the game
'''
def draw(game, screen):
    screen.clear()
    game.draw(screen)
    pygame.display.flip()


def exit_game():
    pygame.quit()
    sys.exit(0)


