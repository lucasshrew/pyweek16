
import data
import director


class Intro(director.Scene):
    
    def __init__(self):
        super(Intro, self).__init__()
        self.background = data.load_image('splash')
        
        director.Director.get().push(self)

    def draw(self, screen):
        screen.blit(self.background)


