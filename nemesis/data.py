'''Simple data loader module.

Loads data files from the "data" directory shipped with the game.

'''

import os

import pygame
from pygame.locals import *

data_py = os.path.abspath(os.path.dirname(__file__))

if 'site-packages.zip' in data_py:
    data_dir = os.path.normpath(os.path.join(os.getcwd(), 'data'))
else:
    data_dir = os.path.normpath(os.path.join(data_py, '..', 'data'))


def filepath(filename):
    '''Determine the path to a file in the data directory.

        Note: it doesnt check for file existance
    '''
    return os.path.join(data_dir, filename)


def load(filename, mode='rb'):
    '''Open a file in the data directory.

        "mode" is passed as the second arg to open().
    '''
    return open(os.path.join(data_dir, filename), mode)


def load_image(filename, ext='.png', colorkey=None):
    '''Loads and returns an pygame image surface

        "ext" is the image extension, defaults to .png
        "colorkey" is the transparent color for the image, defaults to
        none (no transparent color or image has alpha).
    '''
    filename, extension = os.path.splitext(filename)
    ext = extension or ext
    img = pygame.image.load(filepath(filename+ext))
    if colorkey:
        img.set_colorkey(colorkey, RLEACCEL)
        img = img.convert()
    return img

